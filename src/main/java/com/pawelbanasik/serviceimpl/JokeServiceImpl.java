package com.pawelbanasik.serviceimpl;

import org.springframework.stereotype.Service;

import com.pawelbanasik.service.JokeService;

import guru.springframework.norris.chuck.ChuckNorrisQuotes;

@Service
public class JokeServiceImpl implements JokeService {

	// optimalization trick
	private final ChuckNorrisQuotes chuckNorrisQuotes;

	// optimalization trick
	public JokeServiceImpl() {
		this.chuckNorrisQuotes = new ChuckNorrisQuotes();
	}

	@Override
	public String getRandomQuotes() {
		// I would do it like this (also works)
		// ChuckNorrisQuotes chuckNorrisQuotes = new ChuckNorrisQuotes();
		return chuckNorrisQuotes.getRandomQuote();
	}

}
