package com.pawelbanasik.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.pawelbanasik.service.JokeService;

@Controller
public class JokesController {

	private JokeService jokeService;

	// nie jest potrzebna anotacja bo przez jedyny konstruktor
	// @Autowired
	public JokesController(JokeService jokeService) {
		this.jokeService = jokeService;
	}

	@GetMapping("/")
	public String getJoke(Model model) {
		model.addAttribute("joke", jokeService.getRandomQuotes());
		return "chucknorris";
	}

}
