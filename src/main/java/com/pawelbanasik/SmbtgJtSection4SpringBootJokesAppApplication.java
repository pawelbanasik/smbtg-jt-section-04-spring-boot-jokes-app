package com.pawelbanasik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmbtgJtSection4SpringBootJokesAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmbtgJtSection4SpringBootJokesAppApplication.class, args);
	}
}
